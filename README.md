# Hands on with Satellite 6.5

https://redhatsummitlabs.gitlab.io/hands-on-with-satellite-6-5/

Official site for Red Hat Summit's Satellite 6.5 lab documentation

## Development

Prerequisites
- Requires 'nodejs' and 'npm', tested on `node v10.15.0`

To update site:
- fork and clone project
- update `public/lab-guide.md` with your changes
- npm install
- Run `node markdown_to_html.js`
- html will be in `public/index.html`
- Make an MR and merge
- The site is auto-deployed when merged to master
